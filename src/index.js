import React from "react"
import ReactDOM from "react-dom"
import AppRouter from "./AppRouter"
import { Provider } from "react-redux"
import store from "./Store"

ReactDOM.render(
  <Provider store={store}>
    <div>
      <AppRouter />
    </div>
  </Provider>,
  document.getElementById("root")
)
