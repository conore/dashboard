export const checkUserAuth = () => {
  return sessionStorage.getItem("accessToken") !== null // Return true if user has a token saved
}

export const userToken = () => {
  return sessionStorage.getItem("accessToken")
}

export const userRefreshToken = () => {
  return sessionStorage.getItem("refreshToken")
}

export const userAuth = (accessToken, refreshToken) => {
  sessionStorage.setItem("accessToken", accessToken)
  sessionStorage.setItem("refreshToken", refreshToken)
}

export const userLogout = () => {
  sessionStorage.clear()
}
