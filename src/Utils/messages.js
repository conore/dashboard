const messages = message => {
  switch (message) {
    // Login, Logout & Sign up
    case "email_not_recognize":
      return "L'email est associé à aucun compte"
    case "incorrect_email_format":
      return "Le format de l'email est incorrect"
    case "invalid_credentials":
      return "L'email ou le mot de passe est incorrect"
    case "logged_successfully":
      return "Vous êtes connecté"
    case "signup_successfully":
      return "Vous êtes inscrit"

    // Account
    case "user_infos_modified":
      return "Vos informations ont été mis à jour"
    case "email_already_used":
      return "Cet email est déjà utilisé"

    // Clubs
    case "club_already_exist":
      return "Le club existe déjà"
    case "club_created":
      return "Le club a bien été créé"
    case "club_modified":
      return "Le club a bien été modifié"
    case "club_deleted":
      return "Le club a bien été supprimé"

    // Sessions
    case "session_created":
      return "La session a bien été ajoutée"
    case "session_updated":
      return "La session a bien été modifiée"
    case "session_deleted":
      return "La session a bien été supprimée"
    case "date_cannot_be_lower_than_now":
      return "La date de la session ne peut pas être inférieur à maintenant"
    case "reservation_removed":
      return "La réservation a été annulée"

    // Clients
    case "user_added_in_club":
      return "Client ajouté au club"
    case "user_already_in_club":
      return "Client déjà présent dans le club"

    // General
    case "undefined_error":
      return "Une erreur est survenue, veuillez réessayer plus tard"
    case "missing_fields":
      return "Certains champs n'ont pas été rempli"

    default:
      return message
  }
}

export default messages
