export default function validateInput(data) {
  let response = {
    message: "",
    isValid: false
  }

  if (data.length === 0) {
    response.message = "This field is required"
  }

  if (response.message.length === 0) {
    response.isValid = true
  }

  return response
}
