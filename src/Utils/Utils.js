import moment from "moment"
import momentDurationFormatSetup from "moment-duration-format"
momentDurationFormatSetup(moment)

const Utils = {
  _hoursFormat(duration) {
    if (duration % 60 === 0) {
      return moment(duration).format("H[h]")
    } else if (duration < 60) {
      return `${duration}min`
    } else {
      return moment.duration(duration, "minutes").format("H[h]mm")
    }
  },
  setSelectedClubToConnectToStripe(club_id) {
    sessionStorage.setItem("stripeSelectedClub", club_id)
  },
  getSelectedClubToConnectToStripe() {
    return sessionStorage.getItem("stripeSelectedClub")
  }
}

export default Utils
