import { createStore, combineReducers } from "redux"
import userReducer from "./Reducers/UserReducer"
import toastReducer from "./Reducers/ToastReducer"
import settingsReducer from "./Reducers/SettingsReducer"

export default createStore(
  combineReducers({ userReducer, toastReducer, settingsReducer }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
