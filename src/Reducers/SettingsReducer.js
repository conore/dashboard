let defaultState = {
  selected_club: null
}

const settingsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "UPDATE_SELECTED_CLUB":
      state = {
        ...state,
        selected_club: action.payload
      }
      break
    default:
  }
  return state
}

export default settingsReducer
