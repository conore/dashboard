let cryptoState = {
  list: {},
  search: "",
  selected: {
    symbol: "",
    name: ""
  }
}

const cryptoReducer = (state = cryptoState, action) => {
  switch (action.type) {
    case "UPDATE_SELECTED":
      state = {
        ...state,
        selected: {
          ...state.selected,
          symbol: action.payload
        }
      }
      break
    case "UPDATE_LIST":
      state = {
        ...state,
        list: action.payload
      }
      break
    case "UPDATE_SEARCH":
      state = {
        ...state,
        search: action.payload
      }
      break
    default:
  }
  return state
}

export default cryptoReducer
