import store from "../Store"

let toastState = {
  toasts: []
}

const toastReducer = (state = toastState, action) => {
  switch (action.type) {
    case "ADD_TOAST":
      state = {
        ...state,
        toasts: state.toasts.concat({
          status: action.payload.status,
          message: action.payload.message
        })
      }

      setTimeout(() => {
        store.dispatch({ type: "DELETE_TOAST" })
      }, 4000)

      break
    case "DELETE_TOAST":
      state = {
        ...state,
        toasts: state.toasts.slice(1)
      }
      break
    default:
  }
  return state
}

export default toastReducer
