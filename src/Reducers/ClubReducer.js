let defaultState = {
  name: null,
  sessions: []
}

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case "UPDATE_NAME":
      state = {
        ...state,
        name: action.payload
      }
      break
    case "UPDATE_SESSIONS":
      state = {
        ...state,
        sessions: action.payload
      }
      break
    default:
  }
  return state
}

export default reducer
