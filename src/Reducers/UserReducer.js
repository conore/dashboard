let defaultState = {
  authenticated: false,
  username: null,
  id: null,
  email: null,
  firstname: null,
  lastname: null,
  access_token: null,
  refresh_token: null,
  clubs: []
}

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case "UPDATE_AUTH":
      state = {
        ...state,
        authenticated: action.payload
      }
      break
    case "UPDATE_ID":
      state = {
        ...state,
        id: action.payload
      }
      break
    case "UPDATE_USERNAME":
      state = {
        ...state,
        username: action.payload
      }
      break
    case "UPDATE_EMAIL":
      state = {
        ...state,
        email: action.payload
      }
      break
    case "UPDATE_FIRST_NAME":
      state = {
        ...state,
        firstname: action.payload
      }
      break
    case "UPDATE_LAST_NAME":
      state = {
        ...state,
        lastname: action.payload
      }
      break
    case "UPDATE_ACCESS_TOKEN":
      state = {
        ...state,
        access_token: action.payload
      }
      break
    case "UPDATE_REFRESH_TOKEN":
      state = {
        ...state,
        refresh_token: action.payload
      }
      break
    case "UPDATE_CLUBS":
      state = {
        ...state,
        clubs: action.payload
      }
      break
    default:
  }
  return state
}

export default reducer
