import React, { Component } from "react"
import { Link } from "react-router-dom"
// Utils
import _ from "./../Utils/messages"
// Models
import User from "../Models/User"
// Actions
import toastActions from "../Actions/toastActions"
// Store
import { connect } from "react-redux"
import logoSrc from "./../Images/logo.svg"

class LoginPage extends Component {
  constructor() {
    super()
    this.state = {
      isLoading: false,
      email: "",
      password: ""
    }

    this.submitLogin = this.submitLogin.bind(this)
  }

  componentWillUnmount() {
    this.setState({ isLoading: false })
  }

  fieldsNotEmpty() {
    if (this.state.email.length === 0 || this.state.password.length === 0) {
      this.props.toastActions.add("danger", _("field_empty"))
      return false
    } else {
      return true
    }
  }

  submitAdminLogin(e) {
    e.preventDefault() // Empeche la soumission du formulaire

    User.login("test@test.com", "test")
      .then(() => {
        this.props.toastActions.add("success", _("logged_successfully"))
        this.props.history.push("/")
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
      })
  }

  submitLogin(e) {
    e.preventDefault() // Empeche la soumission du formulaire

    if (this.fieldsNotEmpty()) {
      // Verifie si les champs sont remplis, puis on valide le captcha

      User.login(this.state.email, this.state.password)
        .then(() => {
          this.props.toastActions.add("success", _("logged_successfully"))
          this.props.history.push("/")
        })
        .catch(e => {
          this.props.toastActions.add("danger", _(e))
        })
    }
  }
  render() {
    return (
      <div className="h-screen w-screen">
        <div className="flex flex-col items-center flex-1 h-full justify-center px-4 sm:px-0">
          <div className="flex bg-white rounded-sm border w-full sm:max-w-md sm:mx-0">
            <div className="flex flex-col flex-1 justify-center my-8">
              <h1 className="flex justify-center">
                <img className={"h-9"} src={logoSrc} alt={"Logo"} />
              </h1>
              <div className="w-full mt-4">
                <form className="form-horizontal w-3/4 mx-auto">
                  <div className="flex flex-col mt-4">
                    <input
                      id="email"
                      type="email"
                      name="email"
                      required
                      onChange={e => {
                        this.setState({ email: e.target.value })
                      }}
                      placeholder="Email"
                    />
                  </div>
                  <div className="flex flex-col mt-4">
                    <input
                      id="password"
                      type="password"
                      name="password"
                      autoComplete="on"
                      required
                      onChange={e => {
                        this.setState({ password: e.target.value })
                      }}
                      placeholder="Password"
                    />
                  </div>
                  <div className="flex flex-col mt-4">
                    <button
                      onClick={e => this.submitLogin(e)}
                      className="btn btn-primary"
                    >
                      Se connecter
                    </button>
                  </div>
                  {process.env.REACT_APP_ENV === "dev" ? (
                    <div className="flex flex-col mt-4">
                      <button
                        onClick={e => this.submitAdminLogin(e)}
                        className="btn btn-success"
                      >
                        Super connexion
                      </button>
                    </div>
                  ) : null}
                </form>
                <div className="text-center mt-4">
                  <Link
                    to={"/signup"}
                    className="no-underline hover:underline text-gray-500 text-sm"
                  >
                    Pas encore de compte ? Inscrivez-vous
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.userReducer.token
  }
}
const mapDispatchToProps = dispatch => {
  return {
    toastActions: {
      add: (status, message) => dispatch(toastActions.add(status, message))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
