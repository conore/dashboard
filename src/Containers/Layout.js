import React, { Component } from "react"
import { NavLink } from "react-router-dom"
import Modal from "react-modal"
// Utils
import { userLogout } from "../Utils/Auth"
import _ from "./../Utils/messages"
// Models
import Club from "./../Models/Club"
import User from "../Models/User"
// Actions
import settingsActions from "../Actions/settingsActions"
import userActions from "../Actions/userActions"
import toastActions from "../Actions/toastActions"
// Store
import { connect } from "react-redux"
// Images
import logoSrc from "./../Images/logo.svg"

class Layout extends Component {
  constructor(props) {
    super(props)
    this.logout = this.logout.bind(this)
    this.toggleClass = this.toggleClass.bind(this)
    this.state = {
      isLoading: false,
      menuOpen: false,
      showModal: false,
      searchOpen: false,
      openCreateClubModal: false,
      activeIndex: 0,
      club_name: "",
      clubs: [],
      search: "",
      searchResults: []
    }

    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleCloseModal = this.handleCloseModal.bind(this)

    this.getUser()
    this.getClubs()
  }

  async getUser() {
    if (this.props.user.email === null) {
      const user = await User.get()
      this.props.userActions.updateFirstName(user.firstname)
      this.props.userActions.updateLastName(user.lastname)
      this.props.userActions.updateEmail(user.email)
      this.props.userActions.updateId(user.id)
    }
  }
  async getClubs() {
    const clubs = await Club.getAll()
    if (clubs.length === 0) {
      this.handleOpenModal()
    } else {
      this.props.userActions.updateClubs(clubs)
      const current_selected_club = this.props.settings.selected_club
      if (current_selected_club === null) {
        this.props.settingsActions.updateSelectedClub(
          clubs[clubs.length - 1].id
        )
      }
      this.setState({ clubs: clubs })
    }
  }

  async componentDidMount() {
    Modal.setAppElement("body")
  }

  handleOpenModal() {
    this.setState({ showModal: true })
  }

  handleCloseModal() {
    this.setState({ showModal: false })
  }

  toggleClass(index) {
    this.setState({ activeIndex: index })
  }

  handleCreateClub(e) {
    e.preventDefault() // Empeche la soumission du formulaire

    if (this.state.club_name.length === 0)
      return this.props.toastActions.add("danger", _("field_empty"))

    Club.create(this.state.club_name)
      .then(message => {
        this.props.toastActions.add("success", _(message))

        this.getClubs()

        this.handleCloseModal()
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
      })
  }
  async logout() {
    User.logout()
    userLogout()
    window.location.href = "/login"
  }
  render() {
    return (
      <div>
        <div className="header">
          <Modal
            isOpen={this.state.showModal}
            onRequestClose={this.handleCloseModal}
            contentLabel="Créer un club"
            className="modal-default"
          >
            <div className="w-full">
              <button className="float-right" onClick={this.handleCloseModal}>
                <i className="fas fa-times p-2"></i>
              </button>
              <h2 className="text-xl text-center mb-8">Créer un club</h2>

              <form className="form-horizontal mx-auto">
                <div className="flex flex-col mt-4">
                  <input
                    type="text"
                    name="club_name"
                    required
                    onChange={e => {
                      this.setState({ club_name: e.target.value })
                    }}
                    placeholder="Nom du club"
                    onKeyDown={e =>
                      e.key === "Enter" ? this.handleCreateClub(e) : null
                    }
                  />
                </div>
                <div className="flex justify-between mt-8">
                  <button
                    onClick={() => this.handleCloseModal()}
                    className="btn btn-primary"
                  >
                    Annuler
                  </button>
                  <button
                    onClick={e => this.handleCreateClub(e)}
                    className="btn btn-primary"
                  >
                    Créer le club
                  </button>
                </div>
              </form>
            </div>
          </Modal>

          <nav>
            <div id={"logo"} className="flex flex-shrink justify-start">
              <NavLink to={"/"}>
                <img className={"h-6"} src={logoSrc} alt={"Logo"} />
              </NavLink>
            </div>

            <div className="flex flex-row lg:px-21 md:px-24 w-full justify-between sm:w-auto">
              <NavLink to={"/"} exact activeClassName="active" className="link">
                <i className="fas fa-chart-area pr-2"></i>
                <span>Tableau de bord</span>
              </NavLink>
              <NavLink
                to={"/sessions"}
                activeClassName="active"
                className="link"
              >
                <i className="fas fa-clock pr-3"></i>
                <span>Séances</span>
              </NavLink>
              <NavLink
                to={"/clients"}
                activeClassName="active"
                className="link"
              >
                <i className="fa fa-user-friends pr-3"></i>
                <span>Clients</span>
              </NavLink>
            </div>

            <div className="flex flex-1 justify-end py-5 md:py-0">
              <ul className="list-reset flex justify-between md:flex-1 md:flex-none items-center">
                <li>
                  <button
                    className="bg-light hover:bg-light-dark m-1"
                    onClick={this.handleOpenModal}
                  >
                    <i className="fas fa-plus p-2"></i>
                  </button>
                  <div className="inline-block relative w-48">
                    <select
                      value={this.props.settings.selected_club || 0}
                      onChange={e =>
                        this.props.settingsActions.updateSelectedClub(
                          parseInt(e.target.value)
                        )
                      }
                      className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline"
                    >
                      {this.props.user.clubs.map((club, key) => (
                        <option value={club.id} key={key}>
                          {club.name}
                        </option>
                      ))}
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                      <svg
                        className="fill-current h-4 w-4"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                      >
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                      </svg>
                    </div>
                  </div>
                </li>
                <li className="flex-1 md:flex-none">
                  <div className="relative inline-block">
                    <button
                      onClick={() =>
                        this.setState({ menuOpen: !this.state.menuOpen })
                      }
                      className="py-2 pl-4 pr-2 drop-button focus:outline-none"
                    >
                      Bonjour
                      {this.props.user.firstname !== null &&
                      this.props.user.firstname.length > 0
                        ? ", " + this.props.user.firstname
                        : ""}{" "}
                      <svg
                        className="h-3 fill-current inline"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                      >
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                      </svg>
                    </button>
                    <div
                      id="myDropdown"
                      className={
                        "dropdown-menu" +
                        (this.state.menuOpen ? "" : " invisible")
                      }
                    >
                      <NavLink to={"/clubs"} className="dropdown-item">
                        <i className="fa fa-user-cog fa-fw pr-6"></i>
                        <span>Mes clubs</span>
                      </NavLink>
                      <NavLink to={"/account"} className="dropdown-item">
                        <i className="fa fa-user-cog fa-fw pr-6"></i>
                        <span>Compte</span>
                      </NavLink>
                      <button
                        onClick={() => this.logout()}
                        className="dropdown-item"
                      >
                        <i className="fas fa-sign-out-alt fa-fw pr-6"></i>
                        <span>Se déconnecter</span>
                      </button>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>

        <div className="page-content">{this.props.children}</div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.userReducer,
    settings: state.settingsReducer
  }
}
const mapDispatchToProps = dispatch => {
  return {
    settingsActions: {
      updateSelectedClub: club_id =>
        dispatch(settingsActions.updateSelectedClub(club_id))
    },
    toastActions: {
      add: (status, message) => dispatch(toastActions.add(status, message))
    },
    userActions: {
      updateId: id => dispatch(userActions.updateId(id)),
      updateEmail: email => dispatch(userActions.updateEmail(email)),
      updateFirstName: firstname =>
        dispatch(userActions.updateFirstName(firstname)),
      updateLastName: lastname =>
        dispatch(userActions.updateLastName(lastname)),
      updateAccessToken: access_token =>
        dispatch(userActions.updateAccessToken(access_token)),
      updateRefreshToken: refresh_token =>
        dispatch(userActions.updateRefreshToken(refresh_token)),
      updateClubs: clubs => dispatch(userActions.updateClubs(clubs))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout)
