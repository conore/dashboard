import React, { Component } from "react"
// Components
import Layout from "./Layout"
// Models
import Club from "./../Models/Club"
// Actions
import toastActions from "../Actions/toastActions"
// Store
import { connect } from "react-redux"
import Loader from "./../Components/Loader"
import Modal from "react-modal"
import ClientModal from "../Components/Modals/Client"
import ClientInfoModal from "../Components/Modals/ClientInfo"
import _ from "../Utils/messages"

class ClientsPage extends Component {
  state = {
    clients: [],
    modalOpen: "",
    modalData: null,
    isLoading: true
  }
  async componentDidMount() {
    this.refreshClients()
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.settings.selected_club !== this.props.settings.selected_club
    ) {
      this.refreshClients()
    }
  }

  handleToggleModal(modal = "", data = null) {
    this.setState({ modalOpen: modal, modalData: data })
  }

  async refreshClients() {
    this.setState({ isLoading: true })
    const selected_club =
      this.props.settings.selected_club || (await Club.getAll())[0]?.id || null

    const clients =
      selected_club !== null ? await Club.getAllClients(selected_club) : []

    this.setState({ clients: clients })
    this.setState({ isLoading: false })
  }

  renderSwitch() {
    switch (this.state.modalOpen) {
      case "addClient":
        return (
          <ClientModal
            title={"Ajouter un client"}
            button={"Ajouter le client"}
            onValidate={data => this.handleAddClient(data)}
            onCancel={() => this.handleToggleModal()}
          />
        )
      case "showClientInfo":
        return (
          <ClientInfoModal
            title={"Informations du client"}
            button={"Ajouter le client"}
            data={this.state.modalData}
            onCancel={() => this.handleToggleModal()}
          />
        )
      default:
        return null
    }
  }

  handleAddClient(data) {
    if (data.email.length === 0)
      return this.props.toastActions.add("danger", _("field_empty"))

    this.setState({ isLoading: true })
    Club.addClient(
      this.props.settings.selected_club,
      data.firstname,
      data.lastname,
      data.email
    )
      .then(response => {
        this.props.toastActions.add("success", _(response.message))

        this.refreshClients()
        this.handleToggleModal()

        if (response.data !== null) {
          this.handleToggleModal("showClientInfo", response.data)
        }
      })
      .catch(e => {
        this.refreshClients()
        this.props.toastActions.add("danger", _(e))
      })
  }

  render() {
    if (this.state.isLoading === true)
      return (
        <Layout>
          <Loader />
        </Layout>
      )

    const jsx = this.state.clients.map((user, key) => (
      <tr key={key} className="hover:bg-grey-lighter">
        <td className="py-4 px-6 border-b border-grey-light">{user.id}</td>
        <td className="py-4 px-6 border-b border-grey-light">{user.email}</td>
        <td className="py-4 px-6 border-b border-grey-light">
          {user.firstname}
        </td>
        <td className="py-4 px-6 border-b border-grey-light">
          {user.lastname}
        </td>
      </tr>
    ))

    return (
      <Layout>
        <Modal
          isOpen={this.state.modalOpen.length !== 0}
          onRequestClose={() => this.handleToggleModal()}
          className={"modal-default"}
        >
          {this.renderSwitch()}
        </Modal>

        <div className="flex justify-between pt-10 pb-3">
          <h3 className="page-title">Clients</h3>
          <button
            className="btn btn-primary"
            onClick={() => this.handleToggleModal("addClient")}
          >
            Ajouter un client
          </button>
        </div>

        <div className="bg-white shadow-md rounded my-6">
          <table className="text-left w-full border-collapse">
            <thead>
              <tr>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  ID
                </th>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Email
                </th>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Prénom
                </th>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Nom
                </th>
              </tr>
            </thead>
            <tbody>{jsx}</tbody>
          </table>
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    settings: state.settingsReducer
  }
}
const mapDispatchToProps = dispatch => {
  return {
    toastActions: {
      add: (status, message) => dispatch(toastActions.add(status, message))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClientsPage)
