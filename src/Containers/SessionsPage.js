import React, { Component } from "react"
import Modal from "react-modal"
import moment from "moment"
// Components
import Layout from "./Layout"
import Loader from "./../Components/Loader"
// Services
import _ from "./../Utils/messages"
// Models
import Club from "./../Models/Club"
import Session from "./../Models/Session"
// Store
import { connect } from "react-redux"
import toastActions from "../Actions/toastActions"

import SessionModal from "../Components/Modals/Session"
import ReservationsModal from "../Components/Modals/Reservations"
import Utils from "../Utils/Utils"

class SessionsPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      modalOpen: "",
      modalData: null,
      sessions: [],
      reservations: [],
      isLoading: true
    }

    this.handleToggleModal = this.handleToggleModal.bind(this)
  }

  async componentDidMount() {
    this.refreshSessions()
  }

  handleCreateSession(data) {
    if (
      data.places.length === 0 ||
      data.date.length === 0 ||
      data.duration.length === 0
    )
      return this.props.toastActions.add("danger", _("field_empty"))

    this.setState({ isLoading: true })
    Session.create(
      this.props.settings.selected_club,
      data.places,
      data.duration,
      data.date
    )
      .then(message => {
        this.props.toastActions.add("success", _(message))
        this.refreshSessions()
        this.handleToggleModal()
      })
      .catch(e => {
        this.refreshSessions()
        this.props.toastActions.add("danger", _(e))
      })
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.settings.selected_club !== this.props.settings.selected_club
    ) {
      this.refreshSessions()
    }
  }

  handleUpdateSession(data) {
    if (
      data.session_id.length === 0 ||
      data.places.length === 0 ||
      data.date.length === 0 ||
      data.duration.length === 0
    )
      return this.props.toastActions.add("danger", _("field_empty"))

    this.setState({ isLoading: true })
    Session.update(
      this.props.settings.selected_club,
      data.session_id,
      data.places,
      data.duration,
      data.date
    )
      .then(message => {
        this.props.toastActions.add("success", _(message))
        this.refreshSessions()
        this.handleToggleModal()
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
        this.refreshSessions()
      })
  }

  handleDeleteSession(session_id) {
    this.setState({ isLoading: true })
    Session.delete(this.props.settings.selected_club, session_id)
      .then(message => {
        this.props.toastActions.add("success", _(message))
        this.refreshSessions()
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
        this.refreshSessions()
      })
  }

  handleDeleteReservation(reservation_id) {
    this.setState({ isLoading: true })
    Session.deleteReservation(
      this.props.settings.selected_club,
      this.state.modalData.session_id,
      reservation_id
    )
      .then(message => {
        this.props.toastActions.add("success", _(message))
        this.refreshSessions()
        this.handleToggleModal()
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
        this.refreshSessions()
      })
  }

  async refreshSessions() {
    this.setState({ isLoading: true })
    const selected_club = this.props.settings.selected_club || null

    const sessions =
      selected_club !== null ? await Club.getAllSessions(selected_club) : []

    this.setState({ sessions: sessions })
    this.setState({ isLoading: false })
  }

  handleToggleModal(modal = "", data = null) {
    this.setState({ modalOpen: modal, modalData: data })
  }

  async handleOpenModalReservations(session_id) {
    this.setState({ isLoading: true })
    const reservations = await Session.getAllReservations(
      this.props.settings.selected_club,
      session_id
    )
    this.setState({ isLoading: false })
    this.handleToggleModal("showReservations", {
      reservations: reservations,
      session_id: session_id
    })
  }

  renderSwitch() {
    switch (this.state.modalOpen) {
      case "createSession":
        return (
          <SessionModal
            title={"Ajouter une session"}
            button={"Ajouter la session"}
            data={this.state.modalData}
            onValidate={data => this.handleCreateSession(data)}
            onCancel={() => this.handleToggleModal()}
          />
        )
      case "editSession":
        return (
          <SessionModal
            title={"Modifier une session"}
            button={"Modifier la session"}
            data={this.state.modalData}
            onValidate={data => this.handleUpdateSession(data)}
            onCancel={() => this.handleToggleModal()}
          />
        )
      case "showReservations":
        return (
          <ReservationsModal
            title={"Liste des réservations"}
            data={this.state.modalData}
            onDeleteReservation={reservation_id =>
              this.handleDeleteReservation(reservation_id)
            }
            onCancel={() => this.handleToggleModal()}
          />
        )
      default:
        return null
    }
  }

  render() {
    if (this.state.isLoading === true)
      return (
        <Layout>
          <Loader />
        </Layout>
      )

    const jsx = this.state.sessions.map((session, key) => (
      <tr key={key} className="hover:bg-grey-lighter">
        <td
          onClick={() => this.handleOpenModalReservations(session.id)}
          className={"py-4 px-6 border-b border-grey-light text-center"}
        >
          <span
            className={
              session.places - session.remaining_places >= 0
                ? " badge badge-success"
                : " bg-orange-200"
            }
          >
            {session.places - session.remaining_places}/{session.places}
          </span>
        </td>
        <td className="py-4 px-6 border-b border-grey-light text-center">
          {Utils._hoursFormat(session.duration)}
        </td>
        <td className="py-4 px-6 border-b border-grey-light text-center">
          {moment.unix(session.event_at).format("DD/MM/YY à HH[h]mm")}
        </td>
        <td className="py-4 px-6 border-b border-grey-light text-right">
          {session.places - session.remaining_places === 0 ? (
            <button
              className="btn btn-secondary"
              onClick={() => this.handleDeleteSession(session.id)}
            >
              <i className="fas fa-trash p-2"></i>
            </button>
          ) : null}
          <button
            className="btn btn-secondary"
            onClick={() =>
              this.handleToggleModal("editSession", {
                session_id: session.id,
                places: session.places,
                date: session.event_at,
                duration: session.duration
              })
            }
          >
            <i className="fas fa-pen p-2"></i>
          </button>
        </td>
      </tr>
    ))

    return (
      <Layout>
        <Modal
          isOpen={this.state.modalOpen.length !== 0}
          onRequestClose={() => this.handleToggleModal()}
          className={"modal-default"}
        >
          {this.renderSwitch()}
        </Modal>
        <div className="flex justify-between pt-10 pb-3">
          <h3 className="page-title">Séances</h3>
          <button
            className="btn btn-primary"
            onClick={() => this.handleToggleModal("createSession")}
          >
            Ajouter une session
          </button>
        </div>
        <div className="bg-white shadow-md rounded my-6">
          <table className="text-left w-full border-collapse">
            <thead>
              <tr>
                <th className="text-center py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Réservations
                </th>
                <th className="text-center py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Durée
                </th>
                <th className="text-center py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Date
                </th>
                <th className="text-center py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Actions
                </th>
              </tr>
            </thead>
            <tbody>{jsx}</tbody>
          </table>
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    settings: state.settingsReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toastActions: {
      add: (status, message) => dispatch(toastActions.add(status, message))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SessionsPage)
