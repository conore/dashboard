import React, { Component } from "react"
// Components
import Layout from "./Layout"
// Store
import { connect } from "react-redux"
import Chart from "./../Components/Chart"
import toastActions from "../Actions/toastActions"
import Club from "../Models/Club"
import Loader from "./../Components/Loader"

class AppPage extends Component {
  state = {
    isLoading: true,
    clients_number: 0
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.settings.selected_club !== this.props.settings.selected_club
    ) {
      this.refreshStats()
    }
  }

  componentDidMount() {
    this.refreshStats()
  }

  async refreshStats() {
    const selected_club =
      this.props.settings.selected_club || (await Club.getAll())[0]?.id || null

    const clients_number =
      selected_club !== null ? await Club.getClientsCount(selected_club) : 0

    this.setState({ clients_number: 0 })
    this.setState({ clients_number: clients_number })
    this.setState({ isLoading: false })
  }

  render() {
    if (this.state.isLoading === true)
      return (
        <Layout>
          <Loader />
        </Layout>
      )

    return (
      <Layout>
        <div className="flex justify-between pt-10 pb-3">
          <h3 className="page-title">Tableau de bord</h3>
        </div>

        <div className="flex flex-wrap">
          <div className="w-full md:w-1/2 xl:w-1/3 p-3 pl-0">
            <div className="bg-white rounded-sm p-5 border">
              <h5 className="font-bold uppercase text-gray-600 text-sm">
                Revenus (mois)
              </h5>
              <h3 className="font-bold text-3xl">5 619 €</h3>
            </div>
          </div>
          <div className="w-full md:w-1/2 xl:w-1/3 p-3">
            <div className="bg-white rounded-sm p-5 border">
              <h5 className="font-bold uppercase text-gray-600 text-sm">
                Adhérents
              </h5>
              <h3 className="font-bold text-3xl">
                {this.state.clients_number}
              </h3>
            </div>
          </div>
          <div className="w-full md:w-1/2 xl:w-1/3 p-3 pr-0">
            <div className="bg-white rounded-sm p-5 border">
              <h5 className="font-bold uppercase text-gray-600 text-sm">
                Taux de réservation
              </h5>
              <h3 className="font-bold text-3xl">57%</h3>
            </div>
          </div>
        </div>
        <div className="chart-container mt-16">
          <Chart />
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    settings: state.settingsReducer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toastActions: {
      add: (status, message) => dispatch(toastActions.add(status, message))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppPage)
