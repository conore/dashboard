import React, { Component } from "react"

// Components
import Layout from "./Layout"
import Loader from "../Components/Loader"
// Utils
import _ from "../Utils/messages"
// Models
import User from "../Models/User"
// Store
import { connect } from "react-redux"
import toastActions from "../Actions/toastActions"
import userActions from "../Actions/userActions"

class AccountPage extends Component {
  constructor() {
    super()
    this.state = {
      username: "",
      firstname: "",
      lastname: "",
      email: "",
      isLoading: true
    }
  }

  async componentDidMount() {
    const user = await User.get()
    this.setState({
      username: user.username || "",
      firstname: user.firstname || "",
      lastname: user.lastname || "",
      email: user.email || "",
      isLoading: false
    })
  }

  async updateInformation(e) {
    e.preventDefault() // Empeche la soumission du formulaire

    try {
      const response = await User.updateInformations(this.state)

      this.props.toastActions.add("success", _(response))
    } catch (e) {
      this.props.toastActions.add("danger", _(e))
    }
  }

  render() {
    if (this.state.isLoading === true)
      return (
        <Layout>
          <Loader />
        </Layout>
      )

    return (
      <Layout>
        <div className="flex justify-between pt-10 pb-3">
          <h3 className="page-title">Compte</h3>
        </div>

        <div className="flex justify-center mt-6 flex-col">
          <div className="w-full mt-4">
            <form className="form-horizontal w-2/4 mx-auto">
              <div className="flex flex-col mt-4">
                <label htmlFor="firstname">Pseudo</label>
                <input
                  name="text"
                  type="text"
                  value={this.state.username}
                  onChange={e => this.setState({ username: e.target.value })}
                />
              </div>
              <div className="flex flex-col mt-4">
                <label htmlFor="firstname">Prénom</label>
                <input
                  name="text"
                  type="text"
                  value={this.state.firstname}
                  onChange={e => this.setState({ firstname: e.target.value })}
                />
              </div>
              <div className="flex flex-col mt-4">
                <label htmlFor="firstname">Nom</label>
                <input
                  name="text"
                  type="text"
                  value={this.state.lastname}
                  onChange={e => this.setState({ lastname: e.target.value })}
                />
              </div>
              <div className="flex flex-col mt-4">
                <label htmlFor="firstname">Email</label>
                <input
                  name="text"
                  type="text"
                  value={this.state.email}
                  onChange={e => this.setState({ email: e.target.value })}
                />
              </div>
              <div className="flex justify-center mt-5">
                <button
                  className="btn btn-primary"
                  onClick={e => this.updateInformation(e)}
                >
                  Mettre à jour
                </button>
              </div>
            </form>
          </div>
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.userReducer.token
  }
}
const mapDispatchToProps = dispatch => {
  return {
    toastActions: {
      add: (status, message) => dispatch(toastActions.add(status, message))
    },
    userActions: {
      updateId: id => dispatch(userActions.updateId(id)),
      updateEmail: email => dispatch(userActions.updateEmail(email)),
      updateFirstName: firstname =>
        dispatch(userActions.updateFirstName(firstname)),
      updateLastName: lastname =>
        dispatch(userActions.updateLastName(lastname)),
      updateAccessToken: access_token =>
        dispatch(userActions.updateAccessToken(access_token)),
      updateRefreshToken: refresh_token =>
        dispatch(userActions.updateRefreshToken(refresh_token)),
      updateClubs: clubs => dispatch(userActions.updateClubs(clubs))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountPage)
