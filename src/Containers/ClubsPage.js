import React, { Component } from "react"
// Components
import Layout from "./Layout"
// Actions
import toastActions from "../Actions/toastActions"
// Store
import { connect } from "react-redux"
import Club from "../Models/Club"
import Loader from "./../Components/Loader"
import ClubModal from "../Components/Modals/Club"
import ClubPlansModal from "../Components/Modals/ClubPlans"

import _ from "../Utils/messages"
import Modal from "react-modal"
import Stripe from "../Models/Stripe"
import qs from "qs"
import Utils from "../Utils/Utils"

class ClubsPage extends Component {
  state = {
    modalOpen: "",
    modalData: null,
    clubs: [],
    isLoading: true,
    client_id: null
  }
  constructor(props) {
    super(props)
    this.handleClickStripeConnect = this.handleClickStripeConnect.bind(this)
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.settings.selected_club !== this.props.settings.selected_club
    ) {
      this.refreshClubs()
    }
  }
  async componentDidMount() {
    let code = qs.parse(this.props.location.search).code

    if (code !== undefined && code.length > 0) {
      try {
        const club_id = parseInt(Utils.getSelectedClubToConnectToStripe())
        await Stripe.sendStripeCode(club_id, code)
        window.location.href =
          window.location.protocol +
          "//" +
          window.location.host +
          window.location.pathname
      } catch (e) {
        // Erreur
      }
    }

    this.getClientId()
    this.refreshClubs()
  }
  handleToggleModal(modal = "", data = null) {
    this.setState({ modalOpen: modal, modalData: data })
  }
  async refreshClubs() {
    this.setState({ isLoading: true })
    const clubs = await Club.getAll()
    this.setState({ clubs: clubs })
    this.setState({ isLoading: false })
  }
  handleDeleteClub(club_id) {
    this.setState({ isLoading: true })
    Club.delete(club_id)
      .then(message => {
        this.props.toastActions.add("success", _(message))
        this.refreshClubs()
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
        this.refreshClubs()
      })
  }
  handleCreatePlan(data) {
    this.setState({ isLoading: true })
    Club.createPlan(
      data.club_id,
      data.product,
      data.description,
      data.plan,
      data.amount,
      data.interval
    )
      .then(message => {
        this.props.toastActions.add("success", _(message))
        this.refreshClubs()
        this.refreshClubPlans()
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
        this.refreshClubs()
      })
  }
  renderSwitch() {
    switch (this.state.modalOpen) {
      case "editClub":
        return (
          <ClubModal
            title={"Modifier un club"}
            button={"Modifier le club"}
            data={this.state.modalData}
            onValidate={data => this.handleUpdateClub(data)}
            onCancel={() => this.handleToggleModal()}
          />
        )
      case "clubPlans":
        return (
          <ClubPlansModal
            title={"Liste des plans"}
            button={"Créer le plan"}
            data={this.state.modalData}
            onValidate={data => this.handleCreatePlan(data)}
            onCancel={() => this.handleToggleModal()}
          />
        )
      default:
        return null
    }
  }

  async refreshClubPlans(club_id) {
    this.setState({ isLoading: true })
    const plans = await Club.getPlans(club_id)
    this.setState({ plans: plans })
    this.setState({ isLoading: false })
  }

  async handleOpenModalClubPlans(club_id) {
    await this.refreshClubPlans(club_id)
    this.handleToggleModal("clubPlans", {
      plans: this.state.plans,
      club_id: club_id
    })
  }

  handleUpdateClub(data) {
    if (data.club_id.length === 0 || data.name.length === 0)
      return this.props.toastActions.add("danger", _("field_empty"))

    this.setState({ isLoading: true })
    Club.update(data.club_id, data.name)
      .then(message => {
        this.props.toastActions.add("success", _(message))
        this.refreshClubs()
        this.handleToggleModal()
      })
      .catch(e => {
        this.props.toastActions.add("danger", _(e))
        this.handleToggleModal()
        this.refreshClubs()
      })
  }

  async getClientId() {
    this.setState({ isLoading: true })
    const client_id = await Stripe.getClientId()
    this.setState({ client_id: client_id })
    this.setState({ isLoading: false })
  }

  handleClickStripeConnect(club_id) {
    Utils.setSelectedClubToConnectToStripe(club_id)
    const redirect = `https://connect.stripe.com/oauth/authorize?$redirect_uri=${window.location.href}&response_type=code&client_id=${this.state.client_id}&scope=read_write`
    window.location.href = redirect
  }

  render() {
    if (this.state.isLoading === true)
      return (
        <Layout>
          <Loader />
        </Layout>
      )

    const jsx = this.state.clubs.map((club, key) => (
      <tr key={key} className="hover:bg-grey-lighter">
        <td className="py-4 px-6 border-b border-grey-light">{club.id}</td>
        <td className="py-4 px-6 border-b border-grey-light">{club.name}</td>
        <td className="py-4 px-6 border-b border-grey-light">
          {club.users.length || 0}
        </td>

        <td className="py-4 px-6 border-b border-grey-light text-right">
          {club.stripe_connected === false ? (
            <a
              className={"btn btn-secondary cursor-pointer"}
              onClick={() => this.handleClickStripeConnect(club.id)}
            >
              <i className="fab fa-stripe-s p-2"></i>Connecter à Stripe
            </a>
          ) : (
            <span
              className={"btn btn-secondary bg-green-300 hover:bg-green-300"}
            >
              <i className="fab fa-stripe-s p-2"></i>Connecté à Stripe
            </span>
          )}

          {club.users.length === 0 ? (
            <button
              className="btn btn-secondary"
              onClick={() => this.handleDeleteClub(club.id)}
            >
              <i className="fas fa-trash p-2"></i>
            </button>
          ) : null}

          {club.stripe_connected === true ? (
            <button
              className="btn btn-secondary"
              onClick={() => this.handleOpenModalClubPlans(club.id)}
            >
              Plans
            </button>
          ) : null}

          <button
            className="btn btn-secondary"
            onClick={() =>
              this.handleToggleModal("editClub", {
                club_id: club.id,
                name: club.name
              })
            }
          >
            <i className="fas fa-pen p-2"></i>
          </button>
        </td>
      </tr>
    ))

    return (
      <Layout>
        <Modal
          isOpen={this.state.modalOpen.length !== 0}
          onRequestClose={() => this.handleToggleModal()}
          className={"modal-default"}
        >
          {this.renderSwitch()}
        </Modal>

        <div className="flex justify-between pt-10 pb-3">
          <h3 className="page-title">Mes clubs</h3>
        </div>

        <div className="bg-white shadow-md rounded my-6">
          <table className="text-left w-full border-collapse">
            <thead>
              <tr>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  ID
                </th>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Nom
                </th>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Clients
                </th>
                <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                  Actions
                </th>
              </tr>
            </thead>
            <tbody>{jsx}</tbody>
          </table>
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
    settings: state.settingsReducer
  }
}
const mapDispatchToProps = dispatch => {
  return {
    toastActions: {
      add: (status, message) => dispatch(toastActions.add(status, message))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClubsPage)
