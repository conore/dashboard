import React from "react"
import AppPage from "./Containers/AppPage"
import LoginPage from "./Containers/LoginPage"
import SignupPage from "./Containers/SignupPage"
import AccountPage from "./Containers/AccountPage"
import SessionsPage from "./Containers/SessionsPage"
import ClientsPage from "./Containers/ClientsPage"
import ClubssPage from "./Containers/ClubsPage"

import { Switch, BrowserRouter, Route } from "react-router-dom"

import requireAuth from "./Components/requireAuth"
import Notifications from "./Components/ToastContainer"

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Notifications />
      <Switch>
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/signup" component={SignupPage} />
        <Route exact path="/account" component={requireAuth(AccountPage)} />

        <Route exact path="/sessions" component={requireAuth(SessionsPage)} />
        <Route exact path="/clients" component={requireAuth(ClientsPage)} />
        <Route exact path="/clubs" component={requireAuth(ClubssPage)} />
        <Route exact path="/" component={requireAuth(AppPage)} />
      </Switch>
    </div>
  </BrowserRouter>
)

export default AppRouter
