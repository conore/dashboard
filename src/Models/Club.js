import axios from "./../Utils/Axios"

export default class {
  static async create(name) {
    let response = await axios({
      method: "post",
      url: "/clubs",
      data: {
        name: name
      }
    })

    return response.data.message
  }
  static async addClient(club_id, firstname, lastname, email) {
    let response = await axios({
      method: "post",
      url: "/security/register/customer",
      data: {
        firstname: firstname,
        lastname: lastname,
        email: email,
        club_id: club_id
      }
    })

    return response.data
  }
  static async createPlan(
    club_id,
    product,
    description,
    plan,
    amount,
    interval
  ) {
    let response = await axios({
      method: "post",
      url: `/stripe/${club_id}/plans`,
      data: {
        product_name: product,
        description: description,
        plan_name: plan,
        amount: amount,
        interval: interval
      }
    })

    return response.data.message
  }
  static async getPlans(club_id) {
    let response = await axios({
      method: "get",
      url: `/stripe/${club_id}/plans`
    })

    return response.data.data
  }
  static get(id) {
    return axios.get(`/clubs/${id}`)
  }
  static async getClientsCount(id) {
    const response = await axios.get(`/clubs/${id}/users/count`)
    return response.data.data
  }
  static async getAllClients(id) {
    const response = await axios.get(`/clubs/${id}/users`)
    return response.data.data
  }

  static async getAll() {
    const response = await axios.get("/clubs")
    return response.data.data
  }
  static async getAllSessions(club_id) {
    const response = await axios.get("/clubs/" + club_id + "/sessions")
    return response.data.data
  }
  static async delete(club_id) {
    let response = await axios({
      method: "delete",
      url: "/clubs/" + club_id
    })
    return response.data.message
  }
  static async update(club_id, name) {
    let response = await axios({
      method: "put",
      url: "/clubs/" + club_id,
      data: {
        name: name
      }
    })
    return response.data.message
  }
}
