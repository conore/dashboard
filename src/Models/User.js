import axios from "./../Utils/Axios"
import { userAuth } from "./../Utils/Auth"
import Store from "./../Store"
import userActions from "./../Actions/userActions"

const user = {
  updateUsername: username =>
    Store.dispatch(userActions.updateUsername(username)),
  updateEmail: email => Store.dispatch(userActions.updateEmail(email)),
  updateFirstName: firstname =>
    Store.dispatch(userActions.updateFirstName(firstname)),
  updateLastName: lastname =>
    Store.dispatch(userActions.updateLastName(lastname))
}

class User {
  static async get() {
    const user = await axios.get(`/user`)
    return user.data.data
  }

  static async updateInformations(data) {
    let response = await axios({
      method: "post",
      url: "/user",
      data: data
    })

    user.updateUsername(data.username)
    user.updateEmail(data.email)
    user.updateFirstName(data.firstname)
    user.updateLastName(data.lastname)

    return response.data.message
  }

  static async login(email, password) {
    let response = await axios({
      method: "post",
      url: "/security/login",
      data: {
        email: email,
        password: password
      }
    })

    userAuth(response.data.data.accessToken, response.data.data.refreshToken)
  }

  static async signup(email, password) {
    let response = await axios({
      method: "post",
      url: "/security/register",
      data: {
        email: email,
        password: password
      }
    })

    userAuth(response.data.data.accessToken, response.data.data.refreshToken)
  }

  static async logout() {
    await axios({
      method: "post",
      url: "/security/logout"
    })
  }
}

export default User
