import axios from "./../Utils/Axios"

export default class {
  static async getClientId() {
    const response = await axios.get(`/stripe/client_id`)
    return response.data.data
  }
  static async sendStripeCode(club_id, code) {
    let response = await axios({
      method: "post",
      url: "/stripe/connect/" + club_id,
      data: {
        code: code
      }
    })
    return response.data.message
  }
}
