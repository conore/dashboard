import axios from "./../Utils/Axios"
import moment from "moment"

export default class {
  static async create(club_id, places, duration, event_at) {
    let response = await axios({
      method: "post",
      url: `/clubs/${club_id}/sessions`,
      data: {
        places: places,
        duration: duration,
        event_at: moment(event_at).unix()
      }
    })
    return response.data.message
  }

  static async update(club_id, session_id, places, duration, event_at) {
    let response = await axios({
      method: "put",
      url: `/clubs/${club_id}/sessions/${session_id}`,
      data: {
        places: places,
        duration: duration,
        event_at: moment(event_at).unix()
      }
    })
    return response.data.message
  }

  static async delete(club_id, session_id) {
    let response = await axios({
      method: "delete",
      url: `/clubs/${club_id}/sessions/${session_id}`
    })
    return response.data.message
  }

  static async get(club_id, session_id) {
    const response = await axios.get(`/clubs/${club_id}/sessions/${session_id}`)
    return response.data.data
  }

  static async getAll(club_id) {
    const response = await axios.get(`/clubs/${club_id}/sessions`)
    return response.data.data
  }

  static async createReservation(club_id, session_id) {
    let response = await axios({
      method: "post",
      url: `/clubs/${club_id}/sessions/${session_id}/reservations`
    })
    return response.data.message
  }

  static async deleteReservation(club_id, session_id, reservation_id) {
    let response = await axios({
      method: "delete",
      url: `/clubs/${club_id}/sessions/${session_id}/reservations/${reservation_id}`
    })
    return response.data.message
  }

  static async getAllReservations(club_id, session_id) {
    const response = await axios.get(
      `/clubs/${club_id}/sessions/${session_id}/reservations`
    )
    return response.data.data
  }
}
