export default {
  updateId: id => {
    return {
      type: "UPDATE_ID",
      payload: id
    }
  },

  updateUsername: username => {
    return {
      type: "UPDATE_USERNAME",
      payload: username
    }
  },

  updateEmail: email => {
    return {
      type: "UPDATE_EMAIL",
      payload: email
    }
  },

  updateFirstName: firstname => {
    return {
      type: "UPDATE_FIRST_NAME",
      payload: firstname
    }
  },

  updateLastName: lastname => {
    return {
      type: "UPDATE_LAST_NAME",
      payload: lastname
    }
  },

  updateAccessToken: access_token => {
    return {
      type: "UPDATE_ACCESS_TOKEN",
      payload: access_token
    }
  },

  updateRefreshToken: refresh_token => {
    return {
      type: "UPDATE_ACCESS_TOKEN",
      payload: refresh_token
    }
  },

  updateClubs: clubs => {
    return {
      type: "UPDATE_CLUBS",
      payload: clubs
    }
  }
}
