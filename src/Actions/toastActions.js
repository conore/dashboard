export default {
  add: (status, message) => {
    return {
      type: "ADD_TOAST",
      payload: {
        status,
        message
      }
    }
  }
}
