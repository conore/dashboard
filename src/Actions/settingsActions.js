export default {
  updateSelectedClub: club_id => {
    return {
      type: "UPDATE_SELECTED_CLUB",
      payload: club_id
    }
  }
}
