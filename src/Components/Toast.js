import React, { Component } from "react"

const status = {
  danger: "Erreur",
  success: "Succès"
}

class Toast extends Component {
  render() {
    return (
      <div className="toast__cell">
        <div className={"toast toast--" + this.props.status + " add-margin"}>
          <div className="toast__icon"></div>
          <div className="toast__content">
            <p className="toast__type">{status[this.props.status]}</p>
            <p className="toast__message">{this.props.message}</p>
          </div>
          <div className="toast__close"></div>
        </div>
      </div>
    )
  }
}

export default Toast
