import React, { Component } from "react"
import ReactCSSTransitionGroup from "react-addons-css-transition-group"

// Components
import Toast from "./Toast"

// Store
import { connect } from "react-redux"

class ToastContainer extends Component {
  render() {
    let toasts = this.props.toasts.map((toast, key) => {
      return <Toast key={key} status={toast.status} message={toast.message} />
    })
    return (
      <div>
        <div className="toast__container">
          <ReactCSSTransitionGroup
            transitionName="toast"
            transitionEnterTimeout={500}
            transitionLeaveTimeout={500}
          >
            {toasts}
          </ReactCSSTransitionGroup>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    toasts: state.toastReducer.toasts
  }
}

export default connect(mapStateToProps, null)(ToastContainer)
