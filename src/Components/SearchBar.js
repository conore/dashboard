import React, { Component } from "react"
import crossImage from "../Images/cross.svg"

class SearchBar extends Component {
  render() {
    let close =
      this.props.search.length > 0 ? (
        <img
          onClick={e => this.props.updateSearch("")}
          src={crossImage}
          alt="cross icon"
        />
      ) : (
        ""
      )

    return (
      <div className="searchBar-container">
        <input
          placeholder="Search a cryptocurrency..."
          value={this.props.search}
          className="searchBar"
          type="text"
          onChange={e => this.props.updateSearch(e.target.value)}
        />
        {close}
      </div>
    )
  }
}

export default SearchBar
