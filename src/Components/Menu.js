import React, { Component } from "react"
import { Link } from "react-router-dom"
import userImage from "./../Images/user.svg"
import settingsImage from "./../Images/settings.svg"
import logoutImage from "./../Images/logout.svg"
import downArrowImage from "./../Images/down-arrow.svg"
import logoHorizontalImage from "./../Images/logo-horizontal.svg"
import { withRouter } from "react-router"
import { userLogout } from "../Utils/Auth"

class Menu extends Component {
  constructor() {
    super()
    this.logout = this.logout.bind(this)
  }

  logout() {
    userLogout()
    this.props.history.push("/login")
  }
  toggleDropdown() {
    document.querySelector(".dropdown").classList.toggle("active")
  }
  render() {
    return (
      <nav className="menu">
        <Link className="logo-container" to={"/"}>
          <img className="logo" src={logoHorizontalImage} alt="" />
        </Link>

        <div className="right">
          <img
            onClick={() => this.toggleDropdown()}
            className="icon"
            src={downArrowImage}
            alt=""
          />
          <div className="dropdown">
            <div
              className="item"
              onClick={() => this.props.history.push("/account")}
            >
              <img className="icon" src={userImage} alt="" />
              Account
            </div>
            <div
              className="item"
              onClick={() => this.props.history.push("/settings")}
            >
              <img className="icon" src={settingsImage} alt="" />
              Settings
            </div>
            <div className="item" onClick={() => this.logout()}>
              <img className="icon" src={logoutImage} alt="" />
              Logout
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default withRouter(Menu)
