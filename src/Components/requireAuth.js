import React, { Component } from "react"
import { checkUserAuth } from "../Utils/Auth"

export default function(ComposedComponent) {
  class Authentication extends Component {
    componentDidMount() {
      if (!checkUserAuth()) {
        this.props.history.push("/login")
      }
    }

    getSnapshotBeforeUpdate() {
      if (!checkUserAuth()) {
        this.props.history.push("/login")
      }
      return null
    }

    componentDidUpdate() {}

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  return Authentication
}
