import React, { Component } from "react"

export default class extends Component {
  render() {
    return (
      <div className="searchBar-container">
        <input
          placeholder="Search a cryptocurrency..."
          value={this.props.search}
          className="searchBar"
          type="text"
          onChange={e => this.props.updateSearch(e.target.value)}
        />
        {close}
      </div>
    )
  }
}
