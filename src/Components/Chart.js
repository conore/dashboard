import React from "react"
import {
  AreaChart,
  Tooltip,
  Area,
  CartesianGrid,
  XAxis,
  YAxis,
  ResponsiveContainer
} from "recharts"
const data = [
  { name: "Janvier", uv: 400, pv: 2400, amt: 2400 },
  { name: "Février", uv: 700, pv: 6400, amt: 2400 },
  { name: "Mars", uv: 542, pv: 6400, amt: 2400 },
  { name: "Avril", uv: 921, pv: 6400, amt: 2400 },
  { name: "Mai", uv: 1210, pv: 6400, amt: 2400 },
  { name: "Juin", uv: 1021, pv: 6400, amt: 2400 },
  { name: "Juillet", uv: 1523, pv: 6400, amt: 2400 }
]

const renderLineChart = () => (
  <ResponsiveContainer width={"100%"} height={290}>
    <AreaChart data={data} margin={{ top: 20, right: 30, left: 0, bottom: 0 }}>
      <XAxis dataKey="name" />
      <YAxis />
      <CartesianGrid strokeDasharray="3 3" />
      <Tooltip />
      <defs>
        <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
          <stop offset="0%" stopColor="#000000" stopOpacity={0.2} />
          <stop offset="100%" stopColor="#000000" stopOpacity={0} />
        </linearGradient>
      </defs>
      <Area
        type="monotone"
        dataKey="uv"
        stroke="#8884d8"
        fillOpacity={1}
        fill="url(#colorUv)"
      />
    </AreaChart>
  </ResponsiveContainer>
)

export default renderLineChart
