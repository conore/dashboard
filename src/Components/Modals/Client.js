import React, { Component } from "react"

export default class extends Component {
  state = {
    firstname: "",
    lastname: "",
    email: ""
  }

  onValidate(e) {
    e.preventDefault()
    this.props.onValidate(this.state)
  }
  render() {
    return (
      <div className="w-full">
        <button className="float-right" onClick={() => this.props.onCancel()}>
          <i className="fas fa-times p-2"></i>
        </button>
        <h2 className="text-xl text-center mb-8">{this.props.title}</h2>

        <form className="form-horizontal mx-auto">
          <div className="flex flex-col mt-4">
            <input
              type="text"
              value={this.state.firstname}
              name="firstname"
              required
              onChange={e => {
                this.setState({ firstname: e.target.value })
              }}
              placeholder="Prénom du client"
            />
          </div>
          <div className="flex flex-col mt-4">
            <input
              type="text"
              value={this.state.lastname}
              name="lastname"
              required
              onChange={e => {
                this.setState({ lastname: e.target.value })
              }}
              placeholder="Nom du client"
            />
          </div>
          <div className="flex flex-col mt-4">
            <input
              type="text"
              value={this.state.email}
              name="email"
              required
              onChange={e => {
                this.setState({ email: e.target.value })
              }}
              placeholder="Email du client"
            />
          </div>
          <div className="flex justify-between mt-8">
            <button
              onClick={() => this.props.onCancel()}
              className="btn btn-primary"
            >
              Annuler
            </button>
            <button
              onClick={e => this.onValidate(e)}
              className="btn btn-primary"
            >
              {this.props.button}
            </button>
          </div>
        </form>
      </div>
    )
  }
}
