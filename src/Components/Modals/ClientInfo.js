import React, { Component } from "react"

export default class extends Component {
  render() {
    return (
      <div className="w-full">
        <button className="float-right" onClick={() => this.props.onCancel()}>
          <i className="fas fa-times p-2"></i>
        </button>
        <h2 className="text-xl text-center mb-8">{this.props.title}</h2>

        <div className="flex flex-col mt-4">
          <table className="text-left w-full border-collapse mx-auto">
            <tr>
              <td className="border px-4 py-2">Email</td>
              <td className="border px-4 py-2">{this.props.data.email}</td>
            </tr>
            <tr>
              <td className="border px-4 py-2">Mot de passe</td>
              <td className="border px-4 py-2">{this.props.data.password}</td>
            </tr>
          </table>
        </div>

        <div className="flex justify-end mt-8">
          <button
            onClick={() => this.props.onCancel()}
            className="btn btn-primary"
          >
            Fermer
          </button>
        </div>
      </div>
    )
  }
}
