import React, { Component } from "react"
import moment from "moment"

export default class extends Component {
  constructor(props) {
    super(props)
    let reservations = []

    if (props.data !== undefined && props.data !== null) {
      if (props.data.reservations !== undefined) {
        reservations = props.data.reservations
      }
    }

    this.state = {
      reservations
    }
  }

  render() {
    const jsx = this.state.reservations.map((reservation, key) => (
      <tr key={key}>
        <td className="border px-4 py-2">{reservation.id}</td>
        <td className="border px-4 py-2">
          {moment.unix(reservation.created_at).format("DD/MM/YY à HH[h]mm")}
        </td>
        <td className="border px-4 py-2">
          {reservation.user.firstname || "-"}
        </td>
        <td className="border px-4 py-2">{reservation.user.lastname || "-"}</td>
        <td className="py-4 px-6 border-b border-grey-light text-right">
          <button
            className="btn btn-secondary"
            onClick={() => this.props.onDeleteReservation(reservation.id)}
          >
            <i className="fas fa-trash p-2"></i>
          </button>
        </td>
      </tr>
    ))

    return (
      <div className="w-full">
        <button className="float-right" onClick={() => this.props.onCancel()}>
          <i className="fas fa-times p-2"></i>
        </button>
        <h2 className="text-xl text-center mb-8">{this.props.title}</h2>

        <form className="form-horizontal mx-auto">
          {this.state.reservations.length > 0 ? (
            <div className="bg-white shadow-md rounded my-6">
              <table className="text-left w-full border-collapse">
                <thead>
                  <tr>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      ID
                    </th>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      Date
                    </th>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      Prénom
                    </th>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      Nom
                    </th>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody>{jsx}</tbody>
              </table>
            </div>
          ) : (
            <p className={"text-center"}>Aucune réservation pour le moment</p>
          )}
          <div className="flex justify-end mt-8">
            <button
              onClick={() => this.props.onCancel()}
              className="btn btn-primary"
            >
              Fermer
            </button>
          </div>
        </form>
      </div>
    )
  }
}
