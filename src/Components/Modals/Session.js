import React, { Component } from "react"

import { DatetimePickerTrigger } from "rc-datetime-picker"
import moment from "moment"

const shortcuts = {
  "Aujoud'hui": moment(),
  Hier: moment().subtract(1, "days")
}

export default class extends Component {
  constructor(props) {
    super(props)
    let session_id = null
    let date = moment().set({ h: 18, m: 0 })
    let duration = 60
    let places = 12

    if (props.data !== undefined && props.data !== null) {
      if (props.data.session_id !== undefined) {
        session_id = props.data.session_id
      }
      if (props.data.date !== undefined) {
        date = moment.unix(props.data.date)
      }
      if (props.data.duration !== undefined) {
        duration = props.data.duration
      }
      if (props.data.places !== undefined) {
        places = props.data.places
      }
    }

    this.state = {
      session_id,
      date,
      duration,
      places
    }
  }

  onValidate(e) {
    e.preventDefault()
    this.props.onValidate(this.state)
  }
  render() {
    return (
      <div className="w-full">
        <button className="float-right" onClick={() => this.props.onCancel()}>
          <i className="fas fa-times p-2"></i>
        </button>
        <h2 className="text-xl text-center mb-8">{this.props.title}</h2>

        <form className="form-horizontal mx-auto">
          <DatetimePickerTrigger
            className={
              "flex flex-col mt-4 outline-none flex-grow py-4 px-6 border rounded border-grey-400"
            }
            shortcuts={shortcuts}
            moment={this.state.date}
            onChange={date => this.setState({ date: date })}
          >
            <input
              type="text"
              value={this.state.date.format("DD/MM/YYYY HH[h]mm")}
              readOnly
            />
          </DatetimePickerTrigger>
          <div className="flex flex-col mt-4">
            <input
              type="number"
              value={this.state.places}
              name="session_places"
              required
              onChange={e => {
                this.setState({ places: parseInt(e.target.value) })
              }}
              placeholder="Nombre de places"
            />
          </div>
          <div className="flex flex-col mt-4">
            <input
              type="number"
              value={this.state.duration}
              name="session_duration"
              required
              onChange={e => {
                this.setState({ duration: parseInt(e.target.value) })
              }}
              placeholder="Durée de la session (minutes)"
            />
          </div>
          <div className="flex justify-between mt-8">
            <button
              onClick={() => this.props.onCancel()}
              className="btn btn-primary"
            >
              Annuler
            </button>
            <button
              onClick={e => this.onValidate(e)}
              className="btn btn-primary"
            >
              {this.props.button}
            </button>
          </div>
        </form>
      </div>
    )
  }
}
