import React, { Component } from "react"

export default class extends Component {
  constructor(props) {
    super(props)
    let plans = []
    let club_id = null

    if (props.data !== undefined && props.data !== null) {
      if (props.data.plans !== undefined) {
        plans = props.data.plans
      }
      if (props.data.club_id !== undefined) {
        club_id = props.data.club_id
      }
    }

    this.state = {
      club_id,
      plans,
      product: "",
      description: "",
      plan: "",
      amount: "",
      interval: "day"
    }
  }
  onValidate(e) {
    e.preventDefault()
    this.props.onValidate(this.state)
  }
  render() {
    const jsx = this.state.plans.map((plan, key) => (
      <tr key={key}>
        <td className="border px-4 py-2">{plan.name}</td>
        <td className="border px-4 py-2">{plan.product}</td>
        <td className="border px-4 py-2">
          {plan.amount}€/{plan.interval}
        </td>
      </tr>
    ))

    return (
      <div className="w-full">
        <button className="float-right" onClick={() => this.props.onCancel()}>
          <i className="fas fa-times p-2"></i>
        </button>
        <h2 className="text-xl text-center mb-8">{this.props.title}</h2>

        <form className="form-horizontal mx-auto">
          {this.state.plans.length > 0 ? (
            <div className="bg-white shadow-md rounded my-6">
              <table className="text-left w-full border-collapse">
                <thead>
                  <tr>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      Nom
                    </th>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      Produit
                    </th>
                    <th className="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                      Prix
                    </th>
                  </tr>
                </thead>
                <tbody>{jsx}</tbody>
              </table>
            </div>
          ) : (
            <p className={"text-center"}>Aucun plan pour le moment</p>
          )}

          <div className="flex flex-col mt-4">
            <input
              type="text"
              value={this.state.product}
              name="product"
              required
              onChange={e => {
                this.setState({ product: e.target.value })
              }}
              placeholder="Nom du produit"
            />
          </div>
          <div className="flex flex-col mt-4">
            <input
              type="text"
              value={this.state.description}
              name="description"
              required
              onChange={e => {
                this.setState({ description: e.target.value })
              }}
              placeholder="Description"
            />
          </div>
          <div className="flex flex-col mt-4">
            <input
              type="text"
              value={this.state.plan}
              name="plan"
              required
              onChange={e => {
                this.setState({ plan: e.target.value })
              }}
              placeholder="Nom du plan"
            />
          </div>
          <div className="flex flex-col mt-4">
            <input
              type="number"
              value={this.state.amount}
              name="amount"
              required
              onChange={e => {
                this.setState({ amount: e.target.value })
              }}
              placeholder="Prix (€)"
            />
          </div>
          <div className="flex flex-col mt-4">
            <label htmlFor="">Interval</label>
            <select
              name="interval"
              value={this.state.interval}
              onChange={e => this.setState({ interval: e.target.value })}
            >
              <option value="day">Jour</option>
              <option value="week">Semaine</option>
              <option value="month">Mois</option>
              <option value="year">Année</option>
            </select>
          </div>

          <div className="flex justify-between mt-8">
            <button
              onClick={() => this.props.onCancel()}
              className="btn btn-primary"
            >
              Fermer
            </button>
            <button
              onClick={e => this.onValidate(e)}
              className="btn btn-primary"
            >
              {this.props.button}
            </button>
          </div>
        </form>
      </div>
    )
  }
}
