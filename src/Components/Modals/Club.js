import React, { Component } from "react"

export default class extends Component {
  constructor(props) {
    super(props)
    let club_id = null
    let name = ""

    if (props.data !== undefined && props.data !== null) {
      if (props.data.club_id !== undefined) {
        club_id = props.data.club_id
      }
      if (props.data.name !== undefined) {
        name = props.data.name
      }
    }

    this.state = {
      club_id,
      name
    }
  }

  onValidate(e) {
    e.preventDefault()
    this.props.onValidate(this.state)
  }
  render() {
    return (
      <div className="w-full">
        <button className="float-right" onClick={() => this.props.onCancel()}>
          <i className="fas fa-times p-2"></i>
        </button>
        <h2 className="text-xl text-center mb-8">{this.props.title}</h2>

        <form className="form-horizontal mx-auto">
          <div className="flex flex-col mt-4">
            <input
              type="text"
              value={this.state.name}
              name="club_name"
              required
              onChange={e => {
                this.setState({ name: e.target.value })
              }}
              placeholder="Nom du club"
            />
          </div>
          <div className="flex justify-between mt-8">
            <button
              onClick={() => this.props.onCancel()}
              className="btn btn-primary"
            >
              Annuler
            </button>
            <button
              onClick={e => this.onValidate(e)}
              className="btn btn-primary"
            >
              {this.props.button}
            </button>
          </div>
        </form>
      </div>
    )
  }
}
