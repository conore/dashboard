module.exports = {
  theme: {
    extend: {
      fontFamily: {
        display: ["DM Sans", "sans-serif"],
        body: ["DM Sans", "sans-serif"]
      },

      colors: {
        main: "#ffffff",
        light: "#edf1f7",
        "light-dark": "#eceef1",

        primary: {
          light: "#3a3732",
          normal: "#201D1A",
          dark: "#1B1613"
        },
        success: {
          light: "#39B75D",
          normal: "#0C873B",
          dark: "#08743C"
        },
        info: {
          light: "#2F98BB",
          normal: "#01628E",
          dark: "#004C7A"
        },
        warning: {
          light: "#39B75D",
          normal: "#0C873B",
          dark: "#08743C"
        }
      }
    }
  }
}
