# Website

## Développement avec Docker (recommandé 🌈)

Aller dans le dossier du site web et démarrer le container Docker
```bash
cd website
docker-compose up -d
```

👉 Aller sur [localhost:3000](http://localhost:3000)

## Productiondocker 

Pour déployer le site en production, on doit d'abord build l'image (si c'est pas déjà)
```bash
docker build -f Dockerfile-prod -t website:prod . 
```

Maintenant on doit lancer un container à partir de notre image
```bash
docker run -dit -p 80:80 website:prod
```

## Utilisation en local

Installer les dépendances
```bash
cd website
yarn install
```

Lancer le site web
```bash
yarn start
```
