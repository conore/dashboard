# Base image
FROM node:12.2.0-alpine

# Create app directory and use it as working directory
RUN mkdir -p /app
WORKDIR /app

ENV API_URL=dev-api.conore.com

COPY . /app
RUN npm install
RUN npm install react-scripts@3.0.1 -g


CMD [ "npm", "start" ]