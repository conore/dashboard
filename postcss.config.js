const purgecss = require("@fullhuman/postcss-purgecss")({
  content: ["./src/**/*.jsx", "./src/**/*.js", "./public/index.html"],
  css: ["./src/Styles/styles.scss"],
  // Include any special characters you're using in this regular expression
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
})
const postcssSass = require("@csstools/postcss-sass")

module.exports = {
  syntax: "postcss-scss",
  plugins: [
    require("postcss-import"),
    require("tailwindcss"),
    postcssSass,
    require("autoprefixer"),
    //purgecss,
    require("cssnano")
  ]
}
